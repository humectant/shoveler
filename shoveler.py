#! python

import requests
import json
import dns.resolver

from ConfigParser import SafeConfigParser

parser = SafeConfigParser()
parser.read('shoveler.ini')

r_uri = parser.get('shoveler', 'r_uri')
r_vhost = parser.get('shoveler', 'r_vhost')
r_username = parser.get('shoveler', 'r_username')
r_password = parser.get('shoveler', 'r_password')
shovel_name = parser.get('shoveler', 'shovel_name')
dns_host = parser.get('shoveler', 'dns_host')
src_uri = parser.get('shoveler', 'src_uri')
src_exchange = parser.get('shoveler', 'src_exchange')
src_queue = parser.get('shoveler', 'src_queue')
src_exchange_key = parser.get('shoveler', 'src_exchange_key')
dst_uri = parser.get('shoveler', 'dst_uri') 
dst_exchange = parser.get('shoveler', 'dst_exchange')
dst_queue = parser.get('shoveler', 'dst_queue')
dst_exchange_key = parser.get('shoveler', 'dst_exchange_key')
ack_mode = parser.get('shoveler', 'ack_mode')
force = parser.getboolean('shoveler', 'force')
dst_override_target = parser.get('shoveler', 'dst_override_target')

# Prepare
refresh_shovel = False 
dst_hosts = []
rabbit_api = r_uri  + '/' + r_vhost + '/' + shovel_name
shovel_data = ({'value' : ({"src-uri" : src_uri, "src-exchange" : src_exchange, 
    "src-exchange-key" : src_exchange_key, "dest-uri" : dst_uri, "dest-exchange" : dst_exchange, "ack-mode" : ack_mode})})

# Get DNS info
def get_targets(domain, type):
    answers = dns.resolver.query(domain, type)
    targets = []
    for rdata in answers: 
        #print 'AMQP target hosts: ', rdata.target, rdata.port
        targets += [str(rdata.target)[: -1] + ':' + str(rdata.port)]
    print 'DNS target: ', targets
    if dst_override_target:
        print 'Override target with ', dst_override_target
        targets = [dst_override_target]
    print 'AMQP target hosts: ', targets
    return targets

# Update AMQP target hosts
def update_shovel_data(data):
    targets = get_targets(dns_host, 'srv')
    template_uri = data['value']['dest-uri']
    #print 'template_uri: ', temlate_uri
    dst_uris = []
    for target in targets: 
        #print target
        dst_uris += [template_uri.replace("TARGET", target)]
    data['value']['dest-uri'] = ",".join(dst_uris)
    print 'Updated shovel: ', data
    return data

# Refresh shovel data
updated_shovel_data = update_shovel_data(shovel_data)
# Convert to JSON 
shovel_json = json.dumps(updated_shovel_data)
#print 'shovel_json: ', shovel_json

print 'Find shovel ', shovel_name, ' in vhost /', r_vhost
response = requests.get(rabbit_api, auth = (r_username, r_password))
if response.status_code == requests.codes.ok:
    if (force): # Shovel exists; force delete it
        print 'Force shovel refresh'
        requests.delete(rabbit_api, auth = (r_username, r_password))
        refresh_shovel = True
    else: # Shovel exists; only delete if value of dest-uri differs
        curr_json = response.json()
        print 'Current shovel: ', curr_json
        old = curr_json['value']['dest-uri']
        new = updated_shovel_data['value']['dest-uri']
        if old != new:
            print 'Shovel targets differ. Current: ', old, ' DNS: ', new
            requests.delete(rabbit_api, auth = (r_username, r_password))
            refresh_shovel = True
else:
    print 'Shovel not found' 

# Create shovel
if refresh_shovel: 
    headers = {"Content-type": "application/json"}
    requests.put(rabbit_api, auth = (r_username, r_password), headers = headers, data = shovel_json)
    print 'Created shovel: ', shovel_name

